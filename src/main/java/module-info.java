module es.MAQUILES {
    requires javafx.controls;
    requires javafx.fxml;
	requires java.sql;

    opens es.MAQUILES to javafx.fxml;
    exports es.MAQUILES;
}