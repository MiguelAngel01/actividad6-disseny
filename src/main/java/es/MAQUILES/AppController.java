package es.MAQUILES;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class AppController implements Initializable {

	@Override
	public void initialize(URL location, ResourceBundle resources) {

	}
	
	@FXML
	private void pantallaRegistro() {
		
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(App.class.getResource("Registro.fxml"));
			
			GridPane escena = loader.load();
			
			Stage stage = new Stage();
			stage.setScene(new Scene(escena));
			stage.show();
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}
	
	@FXML
	private void pantallaFormulario() {
		
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(App.class.getResource("Formulario.fxml"));
			
			GridPane escena = loader.load();
			
            Stage stage = new Stage();
            stage.setScene(new Scene(escena));
            stage.show();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

}
