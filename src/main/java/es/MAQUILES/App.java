package es.MAQUILES;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class App extends Application {

	@Override
	public void start(Stage primaryStage) throws Exception {
		
		primaryStage.setTitle("Css");
		
        try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(App.class.getResource("Login.fxml"));
			
			AnchorPane escena = loader.load();
			primaryStage.setScene(new Scene(escena));
			primaryStage.show();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public static void main(String[] args) {
		
		launch(args);
		
	}

}
