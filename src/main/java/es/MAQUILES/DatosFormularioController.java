package es.MAQUILES;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.text.Text;

public class DatosFormularioController implements Initializable{

	@FXML
	private Text nom;
	@FXML
	private Text ciutat;
	@FXML
	private Text sistema;
	@FXML
	private Text hores;
	@FXML
	private Text comentari;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
		nom.setText(FormularioController.nombreCompleto);
		ciutat.setText(FormularioController.ciudad);
		sistema.setText(FormularioController.sistema);
		hores.setText(FormularioController.horas);
		comentari.setText(FormularioController.comentario);
		
	}

}
