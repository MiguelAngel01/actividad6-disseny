package es.MAQUILES;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.SpinnerValueFactory.IntegerSpinnerValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class FormularioController implements Initializable{

	ObservableList ciudades = FXCollections.observableArrayList("Ibi","Alcoi","Castalla","Banyeres");
	ObservableList sistemas = FXCollections.observableArrayList("Windows","Mac","Linux");
	
	@FXML
	private ToggleGroup sexe;
	@FXML
	private TextField nom;
	@FXML
	private TextField cognom;
	@FXML
	private TextArea comentari;
	@FXML
	private ChoiceBox ciutat;
	@FXML
	private ChoiceBox sistemaOperatiu;
	@FXML
	private Spinner<Integer> hores;
	
	public static String nombreCompleto;
	public static String comentario;
	public static String ciudad;
	public static String sistema;
	public static String horas;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
		ciutat.setValue("Ibi");
		ciutat.setItems(ciudades);
		sistemaOperatiu.setValue("Windows");
		sistemaOperatiu.setItems(sistemas);
		SpinnerValueFactory<Integer> vFactory = new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 24, 1);
		hores.setValueFactory(vFactory);
		
	}

	@FXML
	private void enviar() {
		
		nombreCompleto = nom.getText().toString() + cognom.getText().toString();
		comentario = comentari.getText().toString();
		ciudad = ciutat.getValue().toString();
		sistema = sistemaOperatiu.getValue().toString();
		horas = hores.getValue().toString();
				
        try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(App.class.getResource("DatosFormulario.fxml"));
			
			AnchorPane escena = loader.load();
			
            Stage stage = new Stage();
            stage.setScene(new Scene(escena));
            stage.show();
			
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
}
